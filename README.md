## Instalação do android-sdk no MacOS Sierra

``brew cask install android-sdk``

## Depois coloco nas variáveis de ambiente

``export ANDROID_HOME=/usr/local/share/android-sdk``
``export PATH=$PATH:$ANDROID_HOME/tools``
``export PATH=$PATH:$ANDROID_HOME/tools/bin``
``export PATH=$PATH:$ANDROID_HOME/platform-tools``

## Depois abro terminal e rodo

``$ANDROID_HOME/tools/bin/sdkmanager "tools" "platform-tools" "platforms;android-25" "build-tools;27.0.3" "extras;android;m2repository" "extras;google;m2repository"``

## Para gerar a chave de segurança para um novo APK caso precise rode

``keytool -genkey -v -keystore msa.keystore -alias msa -keyalg RSA -keysize 2048 -validity 10000``

## Com o emulador abertor, caso precise acionar o hotreload rode

``adb shell input keyevent 82``


``npm install --save-dev eslint-config-react-app babel-eslint@^7.2.3``
