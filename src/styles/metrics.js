import { Dimensions } from 'react-native';

const { width, height } = Dimensions.get('window');

export default {
  basePadding: 20,
  baseMargin: 10,
  baseRadius: 3,
  smallSpace: 4,
  title: 18,
  subTitle: 15,
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width
};
