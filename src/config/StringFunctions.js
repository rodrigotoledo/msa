String.prototype.convertServiceTypeTitle = function () {
  return this.replace(/-/g, ' ').toUpperCase().replace(/_/g, ' ').replace(/\s+/g, '-').toUpperCase().replace(/MSA-/g, '');
};
