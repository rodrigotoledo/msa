import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, AsyncStorage } from 'react-native';
import { withNavigation } from 'react-navigation';
import api from 'services/api';

import styles from './styles';

class JobBox extends Component {
  state = {
    loading: false,
    errorMessage: null,
  };

  detailJob = async (job) => {
    this.setState({ loading: true });
    try {
      const authToken = await AsyncStorage.getItem('@ClientKey:auth_token');
      await this.saveJob(job, authToken);
      this.setState({ loading: false });
      this.props.navigation.navigate('DetailJob', { job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: 'Error en la operación',
      });
    }
  };

  saveJob = async (job, authToken) => {
    await api.post('/jobs/checkin', {
      auth_token: authToken,
      id: job.id,
      loading: false,
      errorMessage: null,
    });
  };

  render() {
    return (
      <View style={styles.jobContainer}>
        <View style={styles.row}>
          <View style={styles.rowSpecialAttention}>
            <Text style={styles.title}>{this.props.job.service_type.title.convertServiceTypeTitle()}</Text>
            <Text style={styles.title}>OS #{this.props.job.id}</Text>
            <Text style={styles.title}>Situación: {this.props.job.status.description}</Text>
            <Text style={styles.subTitle}>Descripción: {this.props.job.description}</Text>
          </View>
          <View>
            <View style={styles.addressContainer}>
              <Text style={[styles.address, styles.subTitle, styles.underline]}>Punto: {this.props.job.customer.name}</Text>
              <Text style={[styles.address, styles.subTitle]}>
                {this.props.job.customer.address_line},{' '}
                {this.props.job.customer.address_neighborhood} -{' '}
                {this.props.job.customer.address_city} /{' '}
                {this.props.job.customer.address_state}
              </Text>
            </View>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.detailJob(this.props.job)}
            >
              {this.state.loading ? (
                <ActivityIndicator size="small" color="#fff" />
              ) : (
                <Text style={styles.buttonText}>Proceder con la operación</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
};

export default withNavigation(JobBox);
