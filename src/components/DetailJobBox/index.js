import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, AsyncStorage, Alert } from "react-native";
import { withNavigation } from "react-navigation";
import api from 'services/api';

import styles from './styles';

class DetailJobBox extends Component {
  state = {
    loading: false,
  };

  componentWillMount = () => {
    let job = this.props.navigation.state.params.job;
    this.setState({ job });
    this.savePosition();
  };

  savePosition = async () => {
    this.setState({ loading: true });
    try {
      const authToken = await AsyncStorage.getItem('@ClientKey:auth_token');

      navigator.geolocation.watchPosition((position) => {
        const formdata = new FormData();
        formdata.append('auth_token', authToken);
        formdata.append('latitude',   position.coords.latitude);
        formdata.append('longitude',  position.coords.longitude);

        try {
          api.post('/users/locations/update', formdata);
        } catch (error) {
          Alert.alert('No se pudo sincronizar su ubicación.');
        }
      });
      this.setState({ loading: false });

    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: 'Error en la operación',
      });
    }
  };


  finishJob = async () => {
    this.setState({ loading: true });
    try {
      this.setState({ loading: false });
      let routeTo = null;
      switch (this.state.job.service_type.name) {
        case 'MSA_IT_SITE_SURVEY':
          routeTo = 'FinishJobSiteSurvey';
          break;
        case 'MSA_ELECTRIC_SURVEY':
          routeTo = 'FinishJobEletricSiteSurvey';
          break;
        default:
          routeTo = 'FinishJob';
          break;
      }
      this.props.navigation.navigate(routeTo, { job: this.state.job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: 'Error en la operación',
      });
    }
  };

  closeJob = async (job) => {
    this.setState({ loading: true });
    try {
      this.setState({ loading: false });
      this.props.navigation.navigate('CloseJob', { job });
    } catch (err) {
      this.setState({
        loading: false,
        errorMessage: 'Error en la operación',
      });
    }
  };

  render() {
    return(
      <View>
        <View style={styles.jobContainer}>
          <View style={styles.row}>
            <View style={styles.rowSpecialAttention}>
              <Text style={styles.title}>{this.state.job.service_type.title.convertServiceTypeTitle()}</Text>
              <Text style={styles.title}>OS #{this.state.job.id}</Text>
              <Text style={styles.title}>
                Situación: {this.state.job.status.description}
              </Text>
              <Text style={styles.subTitle}>Descripción: {this.state.job.description}</Text>
              <Text style={styles.subTitle}>Observación: {this.state.job.emitter_comments}</Text>
            </View>
            <View>
              <View style={styles.addressContainer}>
                <Text style={[styles.address, styles.subTitle, styles.underline]}>Punto: {this.state.job.customer.name}</Text>
                <Text style={[styles.address, styles.subTitle]}>
                  {this.state.job.customer.address_line},{' '}
                  {this.state.job.customer.address_neighborhood} -{' '}
                  {this.state.job.customer.address_city} /{' '}
                  {this.state.job.customer.address_state}
                </Text>
              </View>
              <TouchableOpacity style={[styles.button, styles.buttonDontRealize]} onPress={() => this.closeJob(this.state.job)}>
                {this.state.loading ? <ActivityIndicator size="small" color="#fff" /> : <Text style={styles.buttonText}>No Realizar OS</Text>}
              </TouchableOpacity>

              <TouchableOpacity style={[styles.button, styles.buttonRealize]} onPress={() => this.finishJob(this.state.job)}>
                {this.state.loading ? <ActivityIndicator size="small" color="#fff" /> : <Text style={styles.buttonText}>Realizar OS</Text>}
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
};

export default withNavigation(DetailJobBox);
