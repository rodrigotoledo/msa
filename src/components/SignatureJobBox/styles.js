import { StyleSheet } from 'react-native';
import { colors, metrics } from 'styles';

const styles = StyleSheet.create({
  icon: {
    color: colors.primary
  },

  row: {
    flexDirection: "column"
  },

  rowSpecialAttention: {
    borderLeftWidth: metrics.smallSpace + 2,
    borderLeftColor: colors.danger,
    paddingLeft: metrics.smallSpace,
  },

  leftBarDescription: {
    backgroundColor: colors.regular,
    width: metrics.smallSpace,
    marginRight: metrics.smallSpace,
  },

  title: {
    fontSize: metrics.title,
    fontWeight: "bold"
  },

  subTitle: {
    marginTop: metrics.smallSpace,
    fontSize: metrics.title
  },

  user: {
    marginTop: metrics.baseMargin,
    padding: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  detail: {
    backgroundColor: colors.white,
    margin: metrics.smallSpace,
    fontSize: metrics.subTitle
  },

  underline:{
    textDecorationLine: 'underline'
  },

  address:{
    paddingLeft: metrics.baseMargin,
  },

  addressContainer:{
    backgroundColor: colors.lighter
  },

  jobContainer: {
    flex: 1,
    paddingTop: metrics.basePadding,
    marginBottom: metrics.basePadding,
    borderWidth: 1,
    borderRadius: metrics.borderRadius,
    borderColor: colors.dark,
    backgroundColor: colors.white
  },
  button: {
    backgroundColor: colors.primary,
    alignSelf: "stretch",
    height: 38,
    justifyContent: "center",
    alignItems: "center"
  },
  buttonText: {
    fontWeight: "bold",
    color: colors.white,
    fontSize: 14
  },

  signature: {
    flex: 1,
    borderColor: '#000033',
    borderWidth: 1,
  },
  buttonStyle: {
      flex: 1, justifyContent: "center", alignItems: "center", height: 50,
      backgroundColor: "#eeeeee",
      margin: 10
  }
});

export default styles;
