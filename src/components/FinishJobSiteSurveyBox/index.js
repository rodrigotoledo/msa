import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, ScrollView, TextInput, AsyncStorage, Alert, Image, Picker } from "react-native";
import SignatureView from './SignatureView';
import { NavigationActions, withNavigation } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import "moment/locale/pt-br";
import api from 'services/api';
import styles from './styles';
import { colors } from 'styles';

class FinishJobSiteSurveyBox extends Component {

  constructor(props) {
    super(props);
    this.pressed = false;
  }

  state = {
    loading: false,
    isDateTimePickerVisible: false,
		closedAt: 'Esperando el llenado',
    photo1: null,
    photo2: null,
    solucao: '',
    observacao: '',
    observacoes_pendencias: '',
    // Novos campos
    cliente_endereco: '',
    cliente_cidade_pais: '',
    cliente_contato: '',
    cliente_telefone: '',
    cliente_email: '',
    empresa: '',
    temperatura_site: '',
    cliente_3g: '',
    cliente_radio_link: '',
    cliente_frequencia_uso: '',
    canal_uso: '',
    velocidade: '',
    canal_dedicado: '',
    canal_channel: '',
    cliente_satelite: '',
    cliente_satelite_dedicado: '',
    cliente_satelite_compartilhado: '',
    tipo_faixa: '',
    velocidade_canal: '',
    espaco_instalacao: '',
    //
    protocolo: '',
    tecnicos_adicionais: '',
    cliente_nome: '',
    email_default: '',
    produtos: '',
    data: null,
    dataString: null,
    userData: null,
    userDataString: null,
    latitude: null,
    longitude: null,
  };

  componentWillMount = () => {
    let job = this.props.navigation.state.params.job;
    navigator.geolocation.watchPosition((position) => {
      this.setState({latitude: position.coords.latitude.toString()});
      this.setState({longitude: position.coords.longitude.toString()});
    });

    this.setState({ job });
  };


  selectPhoto1Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };
      this.setState({
        photo1: source
      });
    });
  }

  signature = null;

  selectPhoto2Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };
      this.setState({
        photo2: source
      });
    });
  }



  saveJob = async () => {
    if (!this.pressed){
      this.setState({ loading: true });
      this.pressed = true;
      try {
        let occurred_at_pars = this.state.occurred_at.split(' ');
        let occurred_at = occurred_at_pars[0].split('/').reverse().join('-') + ' ' + occurred_at_pars[1];
        occurred_at = moment(occurred_at).add(3, 'hours');

        const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
        const formdata = new FormData();

        const document = {
          solucao: this.state.solucao,
          observacao: this.state.observacao,
          observacoes_pendencias: this.state.observacoes_pendencias,
          protocolo: this.state.protocolo,
          tecnicos_adicionais: this.state.tecnicos_adicionais,
          cliente_nome: this.state.cliente_nome,
          email_default: this.state.email_default,
          produtos: this.state.produtos,
          cliente_endereco: this.state.cliente_endereco,
          cliente_cidade_pais: this.state.cliente_cidade_pais,
          cliente_contato: this.state.cliente_contato,
          cliente_telefone: this.state.cliente_telefone,
          cliente_email: this.state.cliente_email,
          empresa: this.state.empresa,
          temperatura_site: this.state.temperatura_site,
          cliente_3g: this.state.cliente_3g,
          cliente_radio_link: this.state.cliente_radio_link,
          cliente_frequencia_uso: this.state.cliente_frequencia_uso,
          canal_uso: this.state.canal_uso,
          velocidade: this.state.velocidade,
          canal_dedicado: this.state.canal_dedicado,
          canal_channel: this.state.canal_channel,
          cliente_satelite: this.state.cliente_satelite,
          cliente_satelite_dedicado: this.state.cliente_satelite_dedicado,
          cliente_satelite_compartilhado: this.state.cliente_satelite_compartilhado,
          tipo_faixa: this.state.tipo_faixa,
          velocidade_canal: this.state.velocidade_canal,
          espaco_instalacao: this.state.espaco_instalacao
        }

        formdata.append('auth_token', auth_token);
        formdata.append('activity[latitude]', this.state.latitude);
        formdata.append('activity[longitude]', this.state.longitude);
        formdata.append('activity[job_id]', this.state.job.id);
        formdata.append('activity[close_job]', true);
        formdata.append('activity[solved_job]', true);
        formdata.append('activity[occurred_at]', occurred_at.format('YYYY-MM-DD HH:mm'));
        formdata.append('activity[solucao]',     this.state.solucao);
        formdata.append('activity[observacao]',     this.state.observacao);
        formdata.append('activity[observacoes_pendencias]',     this.state.observacoes_pendencias);
        formdata.append('activity[protocolo]',     this.state.protocolo);
        formdata.append('activity[tecnicos_adicionais]',     this.state.tecnicos_adicionais);
        formdata.append('activity[cliente_nome]',     this.state.cliente_nome);
        formdata.append('activity[email_default]',     this.state.email_default);
        formdata.append('activity[produtos]',     this.state.produtos);
        formdata.append('activity[document]',   JSON.stringify(document));
        if (this.state.photo1 != null && !this.state.photo1.uri.didCancel) {
          formdata.append('activity[photo1]', JSON.stringify(this.state.photo1));
        }

        if (this.state.photo2 != null && !this.state.photo2.uri.didCancel) {
          formdata.append('activity[photo2]', JSON.stringify(this.state.photo2));
        }
        formdata.append('activity[signature]', this.state.dataString);
        formdata.append('activity[user_signature]', this.state.userDataString);

        if (this.state.userDataString == null || this.state.dataString == null) {
          throw ("As assinaturas devem ser preenchidas");
        }

        this.setState({ loading: false });
        try {
          const config = { headers: { 'content-type': 'multipart/form-data' } };
          await api.post('/jobs/update', formdata, config);
					Alert.alert('Los datos se enviaron al sistema!');
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'HomeJobs'}),
            ],
          });
          this.props.navigation.dispatch(resetAction);
        } catch (error) {
          this.pressed = false;
          this.setState({loading: false})
					Alert.alert('Compruebe todos los campos, hay algunos errores.');
        }
      } catch (error) {
        this.pressed = false
				Alert.alert('Compruebe todos los campos, hay algunos errores.');
        this.setState({
          loading: false,
					errorMessage: 'Error en la operación',
        });
      }
    }
  }

  _showSignatureView() {
    this._signatureView.show(true);
  }

   _showUserSignatureView() {
     this._userSignatureView.show(true);
   }

  _onSave(result) {
    const blankImage = "iVBORw0KGgoAAAANSUhEUgAAAY8AAAH0CAIAAABKHTayAAAAA3NCSVQFBgUzC42AAAAGeUlEQVR4";
    if (result && !result.encoded.includes(blankImage)) {
      const base64String = `data:image/png;base64,${result.encoded}`;
      const dataString = result.encoded;
      this.setState({data: base64String});
      this.setState({dataString: dataString});
    }else{
      this.setState({data: null});
      this.setState({dataString: null});
    }
    this._signatureView.show(false);
  }

  _onUserSave(result) {
    const blankImage = "iVBORw0KGgoAAAANSUhEUgAAAY8AAAH0CAIAAABKHTayAAAAA3NCSVQFBgUzC42AAAAGeUlEQVR4";
    if (result && !result.encoded.includes(blankImage)) {
      const base64String = `data:image/png;base64,${result.encoded}`;
      const dataString = result.encoded;
      this.setState({userData: base64String});
      this.setState({userDataString: dataString});
    }else{
      this.setState({userData: null});
      this.setState({userDataString: null});
    }
    this._userSignatureView.show(false);

  }


  _onSaveEvent(result) {
    this.setState()
  }
  _onDragEvent() {
  }

  render() {
    const { data, userData } = this.state;
    return (
      <ScrollView>
        <View style={styles.jobContainer}>
          <View style={styles.row}>
            <View style={styles.rowSpecialAttention}>
              <Text style={styles.title}>{this.state.job.service_type.title.replace(/-/g,' ').toUpperCase().replace(/_/g,' ').replace(/\s+/g, '-').toUpperCase()}</Text>
              <Text style={styles.title}>OS #{this.state.job.id}</Text>
              <Text style={styles.title}>
                Situación: {this.state.job.status.description}
              </Text>
							<Text style={styles.subTitle}>Descripción: {this.state.job.description}</Text>
							<Text style={styles.subTitle}>Observación: {this.state.job.emitter_comments}</Text>
            </View>
            <View style={styles.addressContainer}>
							<Text style={[styles.address, styles.subTitle, styles.underline]}>Punto: {this.state.job.customer.name}</Text>
              <Text style={[styles.address, styles.subTitle]}>
                {this.state.job.customer.address_line},{" "}
                {this.state.job.customer.address_neighborhood} -{" "}
                {this.state.job.customer.address_city} /{" "}
                {this.state.job.customer.address_state}
              </Text>
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Fecha y hora de realización</Text>
            <View style={styles.calendarContainer}>
              <DatePicker
                style={{width: 200}}
                date={this.state.occurred_at}
                mode="datetime"
								placeholder="Informe la fecha"
                format="DD/MM/YYYY HH:mm"
                minDate="2016-05-01"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(occurred_at) => {this.setState({occurred_at: occurred_at})}}
              />
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Fotos a presentar</Text>
            <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', flex: 1}}>
              <TouchableOpacity onPress={this.selectPhoto1Tapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
									{this.state.photo1 === null ? <Text>Primera foto</Text> :
                  <Image style={styles.avatar} source={this.state.photo1.uri} />
                }
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.selectPhoto2Tapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                { this.state.photo2 === null ? <Text>Segunda foto</Text> :
                  <Image style={styles.avatar} source={this.state.photo2.uri} />
                }
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Solución Presentada</Text>
						<TextInput value={this.state.solucao} placeholder='Solución' onChangeText={(solucao) => {this.setState({solucao: solucao})}} />
						<TextInput value={this.state.observacao} placeholder='Observación' onChangeText={(observacao) => {this.setState({observacao: observacao})}} />
            <TextInput value={this.state.observacoes_pendencias} placeholder='Pendiente?' onChangeText={(observacoes_pendencias) => {this.setState({observacoes_pendencias: observacoes_pendencias})}} />
            { /* Novos Campos */ }
            <TextInput value={this.state.cliente_endereco} placeholder="Direccion Del Cliente" onChangeText={(cliente_endereco) => {this.setState({cliente_endereco: cliente_endereco})}} />
            <TextInput value={this.state.cliente_cidade_pais} placeholder="Ciudad/pais" onChangeText={(cliente_cidade_pais) => {this.setState({cliente_cidade_pais: cliente_cidade_pais})}} />
            <TextInput value={this.state.cliente_contato} placeholder="Contacto" onChangeText={(cliente_contato) => {this.setState({cliente_contato: cliente_contato})}} />
            <TextInput value={this.state.cliente_telefone} placeholder="Telefono" onChangeText={(cliente_telefone) => {this.setState({cliente_telefone: cliente_telefone})}} />
            <TextInput value={this.state.cliente_email} placeholder="Correo" onChangeText={(cliente_email) => {this.setState({cliente_email: cliente_email})}} />
            <TextInput value={this.state.empresa} placeholder="Nombre De La Empresa" onChangeText={(empresa) => {this.setState({empresa: empresa})}} />
            <TextInput value={this.state.temperatura_site} placeholder="Temperatura En Sitio" onChangeText={(temperatura_site) => {this.setState({temperatura_site: temperatura_site})}} />
            <Text style={[styles.blockTitle, styles.title]}>Cliente Con Cobertura 3G/4G?</Text>
            <Picker
              selectedValue={this.state.cliente_3g}
              prompt='Cliente Con Cobertura 3G/4G?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_3g: itemValue})}>
              <Picker.Item label="Sem cobertura" value="Sem cobertura" />
              <Picker.Item label="Cliente Con Cobertura 3G" value="Cobertura 3G" />
              <Picker.Item label="Cliente Con Cobertura 4G" value="Cobertura 4G" />
            </Picker>
            <TextInput value={this.state.cliente_radio_link} placeholder="Cliente Aplica Para Radio Enlace?" onChangeText={(cliente_radio_link) => {this.setState({cliente_radio_link: cliente_radio_link})}} />
            <TextInput value={this.state.cliente_frequencia_uso} placeholder="Frecuencia A Usar" onChangeText={(cliente_frequencia_uso) => {this.setState({cliente_frequencia_uso: cliente_frequencia_uso})}} />
            <TextInput value={this.state.canal_uso} placeholder="Canal A Usar" onChangeText={(canal_uso) => {this.setState({canal_uso: canal_uso})}} />
            <TextInput value={this.state.velocidade} placeholder="Velocidad De TX/RX" onChangeText={(velocidade) => {this.setState({velocidade: velocidade})}} />
            <TextInput value={this.state.canal_dedicado} placeholder="Canal Dedicado" onChangeText={(canal_dedicado) => {this.setState({canal_dedicado: canal_dedicado})}} />
            <TextInput value={this.state.canal_channel} placeholder="Canal Banda Ancha" onChangeText={(canal_channel) => {this.setState({canal_channel: canal_channel})}} />
            <TextInput value={this.state.cliente_satelite} placeholder="Cliente Aplica Para Canal Satelital?" onChangeText={(cliente_satelite) => {this.setState({cliente_satelite: cliente_satelite})}} />
            <TextInput value={this.state.cliente_satelite_dedicado} placeholder="Satelital Dedicado" onChangeText={(cliente_satelite_dedicado) => {this.setState({cliente_satelite_dedicado: cliente_satelite_dedicado})}} />
            <TextInput value={this.state.cliente_satelite_compartilhado} placeholder="Satelital Dedicado - Compartilhado?" onChangeText={(cliente_satelite_compartilhado) => {this.setState({cliente_satelite_compartilhado: cliente_satelite_compartilhado})}} />
            <TextInput value={this.state.tipo_faixa} placeholder="Tipo De Banda (C, KU, KA)" onChangeText={(tipo_faixa) => {this.setState({tipo_faixa: tipo_faixa})}} />
            <TextInput value={this.state.velocidade_canal} placeholder="Velocidad Del Canal" onChangeText={(velocidade_canal) => {this.setState({velocidade_canal: velocidade_canal})}} />
            <TextInput value={this.state.espaco_instalacao} placeholder="Espacio Para Instalar Equipos De Comuinicaciones?" onChangeText={(espaco_instalacao) => {this.setState({espaco_instalacao: espaco_instalacao})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Protocolo de servicio</Text>
						<TextInput value={this.state.protocolo} placeholder="Número de protocolo" onChangeText={(protocolo) => {this.setState({protocolo: protocolo})}} />
						<TextInput value={this.state.tecnicos_adicionais} placeholder="Técnicos adicionales" onChangeText={(tecnicos_adicionais) => {this.setState({tecnicos_adicionais: tecnicos_adicionais})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
            <Text style={[styles.blockTitle, styles.title]}>Cliente</Text>
						<TextInput value={this.state.cliente_nome} placeholder="Nombre del cliente" onChangeText={(cliente_nome) => {this.setState({cliente_nome: cliente_nome})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Correos eletrónicos que recibirán una copia de OS para la impresión y la firma. Separados por coma (,)</Text>
						<TextInput value={this.state.email_default} placeholder="Correos eletrónicos" onChangeText={(email_default) => {this.setState({email_default: email_default})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Productos utilizados. Separados por coma (,)</Text>
						<TextInput value={this.state.produtos} placeholder="Productos utilizados" onChangeText={(produtos) => {this.setState({produtos: produtos})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Para completar el proceso, las firmas son necesarias</Text>
            <View style={{ flex: 1, flexDirection: "column", alignItems: 'center' }}>
              <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._showSignatureView.bind(this)}>
                    <View style={[styles.flexCenter, {padding: 10}]}>

                      <Text style={styles.signatureText}>
                        {data ? 'Esta es su suscripción' : 'Haga clic aquí para firmar.'}
                      </Text>
                      <View style={{paddingBottom: 10}} />
                      {data &&
                        <View style={{backgroundColor: 'white', }}>
                          <Image
                            resizeMode={'contain'}
                            style={{width: 300, height: 300}}
                            source={{uri: data}}
                          />
                        </View>
                      }
                    </View>
                  </TouchableOpacity>
                  <SignatureView
                    ref={r => this._signatureView = r}
                    rotateClockwise={!!true}
                    onSave={this._onSave.bind(this)}
                  />
              </View>

              <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._showUserSignatureView.bind(this)}>
                    <View style={[styles.flexCenter, {padding: 10}]}>

                      <Text style={styles.signatureText}>
                        {userData ? 'Esta es la firma del técnico' : 'Haga clic aquí para la firma del técnico.'}
                      </Text>
                      <View style={{paddingBottom: 10}} />
                      {userData &&
                        <View style={{backgroundColor: 'white', }}>
                          <Image
                            resizeMode={'contain'}
                            style={{width: 300, height: 300}}
                            source={{uri: userData}}
                          />
                        </View>
                      }
                    </View>
                  </TouchableOpacity>
                  <SignatureView
                    ref={r => this._userSignatureView = r}
                    rotateClockwise={!!true}
                    onSave={this._onUserSave.bind(this)}
                  />
              </View>
            </View>
            <TouchableOpacity style={styles.button} onPress={() => this.saveJob()}>
              {
                this.pressed
                  ? <ActivityIndicator size="small"/>
									: <Text style={styles.buttonText}>Finalizar Atención</Text>
              }
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>);
  }
};

export default withNavigation(FinishJobSiteSurveyBox);
