import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, ScrollView, TextInput, AsyncStorage, Alert, Image, Picker } from "react-native";
import SignatureView from './SignatureView';
import { NavigationActions, withNavigation } from "react-navigation";
import DatePicker from 'react-native-datepicker';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
import "moment/locale/pt-br";
import api from 'services/api';
import styles from './styles';
import { colors } from 'styles';

class FinishJobEletricSiteSurveyBox extends Component {

  constructor(props) {
    super(props);
    this.pressed = false;
  }

  state = {
    tipo_cliente: '',
    tipo_conexao_eletrica: '',
    cliente_disponivel_polo_terra: '',
    tipo_mala_terra: '',
    medida_polo_terra: '',
    potencia_saida_usar: '',
    cliente_disponivel_instalar_gcm: '',
    espaco_disponivel_planta_gcm: '',
    cliente_possui_seguranca_perimetral: '',
    cliente_externo_interno: '',
    instalar_placa_concreto_base: '',
    facilidade_acesso_carretera: '',
    facilidade_acesso_trocha: '',
    facilidade_acesso_area: '',
    facilidade_acesso_maritima: '',
    voltagem: '',
    frequencia: '',
    numero_fases: '',
    fator_potencia: '',
    decibeis: '',
    amperagem: '',
    medida_localizacao_fabrica_cliente: '',
    tipo_transmissao: '',
    cliente_possui_placa_transferencia: '',
    estado_conexoes_internas: '',
    quantidade_equipamentos_energizar: '',
    potencia_real_energetica: '',
    horas_pico_energetica: '',
    trabalham_24: '',



    loading: false,
    isDateTimePickerVisible: false,
		closedAt: 'Esperando el llenado',
    photo1: null,
    photo2: null,
    solucao: '',
    observacao: '',
    observacoes_pendencias: '',
    protocolo: '',
    tecnicos_adicionais: '',
    cliente_nome: '',
    email_default: '',
    produtos: '',
    data: null,
    dataString: null,
    userData: null,
    userDataString: null,
    latitude: null,
    longitude: null
  };

  componentWillMount = () => {
    let job = this.props.navigation.state.params.job;
    navigator.geolocation.watchPosition((position) => {
      this.setState({latitude: position.coords.latitude.toString()});
      this.setState({longitude: position.coords.longitude.toString()});
    });

    this.setState({ job });
  };


  selectPhoto1Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };
      this.setState({
        photo1: source
      });
    });
  }

  signature = null;

  selectPhoto2Tapped() {
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      const source = { uri: response };
      this.setState({
        photo2: source
      });
    });
  }



  saveJob = async () => {
    if (!this.pressed){
      this.setState({ loading: true });
      this.pressed = true;
      try {
        let occurred_at_pars = this.state.occurred_at.split(' ');
        let occurred_at = occurred_at_pars[0].split('/').reverse().join('-') + ' ' + occurred_at_pars[1];
        occurred_at = moment(occurred_at).add(3, 'hours');

        const auth_token = await AsyncStorage.getItem('@ClientKey:auth_token');
        const formdata = new FormData();

        const document = {
          solucao: this.state.solucao,
          observacao: this.state.observacao,
          observacoes_pendencias: this.state.observacoes_pendencias,
          protocolo: this.state.protocolo,
          tecnicos_adicionais: this.state.tecnicos_adicionais,
          cliente_nome: this.state.cliente_nome,
          email_default: this.state.email_default,
          produtos: this.state.produtos,


          tipo_cliente: this.state.tipo_cliente,
          tipo_conexao_eletrica: this.state.tipo_conexao_eletrica,
          cliente_disponivel_polo_terra: this.state.cliente_disponivel_polo_terra,
          tipo_mala_terra: this.state.tipo_mala_terra,
          medida_polo_terra: this.state.medida_polo_terra,
          potencia_saida_usar: this.state.potencia_saida_usar,
          cliente_disponivel_instalar_gcm: this.state.cliente_disponivel_instalar_gcm,
          espaco_disponivel_planta_gcm: this.state.espaco_disponivel_planta_gcm,
          cliente_possui_seguranca_perimetral: this.state.cliente_possui_seguranca_perimetral,
          cliente_externo_interno: this.state.cliente_externo_interno,
          instalar_placa_concreto_base: this.state.instalar_placa_concreto_base,
          facilidade_acesso_carretera: this.state.facilidade_acesso_carretera,
          facilidade_acesso_trocha: this.state.facilidade_acesso_trocha,
          facilidade_acesso_area: this.state.facilidade_acesso_area,
          facilidade_acesso_maritima: this.state.facilidade_acesso_maritima,
          voltagem: this.state.voltagem,
          frequencia: this.state.frequencia,
          numero_fases: this.state.numero_fases,
          fator_potencia: this.state.fator_potencia,
          decibeis: this.state.decibeis,
          amperagem: this.state.amperagem,
          medida_localizacao_fabrica_cliente: this.state.medida_localizacao_fabrica_cliente,
          tipo_transmissao: this.state.tipo_transmissao,
          cliente_possui_placa_transferencia: this.state.cliente_possui_placa_transferencia,
          estado_conexoes_internas: this.state.estado_conexoes_internas,
          quantidade_equipamentos_energizar: this.state.quantidade_equipamentos_energizar,
          potencia_real_energetica: this.state.potencia_real_energetica,
          horas_pico_energetica: this.state.horas_pico_energetica,
          trabalham_24: this.state.trabalham_24,
        };

        formdata.append('auth_token', auth_token);
        formdata.append('activity[latitude]', this.state.latitude);
        formdata.append('activity[longitude]', this.state.longitude);
        formdata.append('activity[job_id]', this.state.job.id);
        formdata.append('activity[close_job]', true);
        formdata.append('activity[solved_job]', true);
        formdata.append('activity[occurred_at]', occurred_at.format('YYYY-MM-DD HH:mm'));
        formdata.append('activity[solucao]', this.state.solucao);
        formdata.append('activity[observacao]', this.state.observacao);
        formdata.append('activity[observacoes_pendencias]', this.state.observacoes_pendencias);
        formdata.append('activity[protocolo]', this.state.protocolo);
        formdata.append('activity[tecnicos_adicionais]', this.state.tecnicos_adicionais);
        formdata.append('activity[cliente_nome]', this.state.cliente_nome);
        formdata.append('activity[email_default]', this.state.email_default);
        formdata.append('activity[produtos]', this.state.produtos);
        formdata.append('activity[document]', JSON.stringify(document));
        if (this.state.photo1 != null && !this.state.photo1.uri.didCancel) {
          formdata.append('activity[photo1]', JSON.stringify(this.state.photo1));
        };

        if (this.state.photo2 != null && !this.state.photo2.uri.didCancel) {
          formdata.append('activity[photo2]', JSON.stringify(this.state.photo2));
        }
        formdata.append('activity[signature]', this.state.dataString);
        formdata.append('activity[user_signature]', this.state.userDataString);

        if (this.state.userDataString == null || this.state.dataString == null) {
          throw ("As assinaturas devem ser preenchidas");
        }

        this.setState({ loading: false });
        try {
          const config = { headers: { 'content-type': 'multipart/form-data' } };
          await api.post('/jobs/update', formdata, config);
					Alert.alert('Los datos se enviaron al sistema!');
          const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
              NavigationActions.navigate({ routeName: 'HomeJobs'}),
            ],
          });
          this.props.navigation.dispatch(resetAction);
        } catch (error) {
          this.pressed = false;
          this.setState({loading: false})
					Alert.alert('Compruebe todos los campos, hay algunos errores.');
        }
      } catch (error) {
        this.pressed = false
				Alert.alert('Compruebe todos los campos, hay algunos errores.');
        this.setState({
          loading: false,
					errorMessage: 'Error en la operación',
        });
      }
    }
  }

  _showSignatureView() {
    this._signatureView.show(true);
  }

   _showUserSignatureView() {
     this._userSignatureView.show(true);
   }

  _onSave(result) {
    const blankImage = "iVBORw0KGgoAAAANSUhEUgAAAY8AAAH0CAIAAABKHTayAAAAA3NCSVQFBgUzC42AAAAGeUlEQVR4";
    if (result && !result.encoded.includes(blankImage)) {
      const base64String = `data:image/png;base64,${result.encoded}`;
      const dataString = result.encoded;
      this.setState({data: base64String});
      this.setState({dataString: dataString});
    }else{
      this.setState({data: null});
      this.setState({dataString: null});
    }
    this._signatureView.show(false);
  }

  _onUserSave(result) {
    const blankImage = "iVBORw0KGgoAAAANSUhEUgAAAY8AAAH0CAIAAABKHTayAAAAA3NCSVQFBgUzC42AAAAGeUlEQVR4";
    if (result && !result.encoded.includes(blankImage)) {
      const base64String = `data:image/png;base64,${result.encoded}`;
      const dataString = result.encoded;
      this.setState({userData: base64String});
      this.setState({userDataString: dataString});
    }else{
      this.setState({userData: null});
      this.setState({userDataString: null});
    }
    this._userSignatureView.show(false);

  }


  _onSaveEvent(result) {
    this.setState()
  }
  _onDragEvent() {
  }

  render() {
    const { data, userData } = this.state;
    return (
      <ScrollView>
        <View style={styles.jobContainer}>
          <View style={styles.row}>
            <View style={styles.rowSpecialAttention}>
              <Text style={styles.title}>{this.state.job.service_type.title.replace(/-/g,' ').toUpperCase().replace(/_/g,' ').replace(/\s+/g, '-').toUpperCase()}</Text>
              <Text style={styles.title}>OS #{this.state.job.id}</Text>
              <Text style={styles.title}>
                Situación: {this.state.job.status.description}
              </Text>
							<Text style={styles.subTitle}>Descripción: {this.state.job.description}</Text>
							<Text style={styles.subTitle}>Observación: {this.state.job.emitter_comments}</Text>
            </View>
            <View style={styles.addressContainer}>
							<Text style={[styles.address, styles.subTitle, styles.underline]}>Punto: {this.state.job.customer.name}</Text>
              <Text style={[styles.address, styles.subTitle]}>
                {this.state.job.customer.address_line},{" "}
                {this.state.job.customer.address_neighborhood} -{" "}
                {this.state.job.customer.address_city} /{" "}
                {this.state.job.customer.address_state}
              </Text>
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Fecha y hora de realización</Text>
            <View style={styles.calendarContainer}>
              <DatePicker
                style={{width: 200}}
                date={this.state.occurred_at}
                mode="datetime"
								placeholder="Informe la fecha"
                format="DD/MM/YYYY HH:mm"
                minDate="2016-05-01"
                confirmBtnText="Confirmar"
                cancelBtnText="Cancelar"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0
                  },
                  dateInput: {
                    marginLeft: 36
                  }
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={(occurred_at) => {this.setState({occurred_at: occurred_at})}}
              />
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Fotos a presentar</Text>
            <View style={{flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', flex: 1}}>
              <TouchableOpacity onPress={this.selectPhoto1Tapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
									{this.state.photo1 === null ? <Text>Primera foto</Text> :
                  <Image style={styles.avatar} source={this.state.photo1.uri} />
                }
                </View>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.selectPhoto2Tapped.bind(this)}>
                <View style={[styles.avatar, styles.avatarContainer, {marginBottom: 20}]}>
                { this.state.photo2 === null ? <Text>Segunda foto</Text> :
                  <Image style={styles.avatar} source={this.state.photo2.uri} />
                }
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Solución Presentada</Text>
						<TextInput value={this.state.solucao} placeholder='Solución' onChangeText={(solucao) => {this.setState({solucao: solucao})}} />
						<TextInput value={this.state.observacao} placeholder='Observación' onChangeText={(observacao) => {this.setState({observacao: observacao})}} />
            <TextInput value={this.state.observacoes_pendencias} placeholder='Pendiente?' onChangeText={(observacoes_pendencias) => {this.setState({observacoes_pendencias: observacoes_pendencias})}} />




            <Text style={[styles.title, styles.textTitle]}>Tipo de Cliente</Text>
            <Picker
              selectedValue={this.state.tipo_cliente}
              prompt='Tipo de Cliente' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({tipo_cliente: itemValue})}>
              <Picker.Item label="Industrial" value="Industrial" />
              <Picker.Item label="Residencial" value="Residencial" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>Tipo de Conexión Electrica</Text>
            <Picker
              selectedValue={this.state.tipo_conexao_eletrica}
              prompt='Tipo de Conexión Electrica' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({tipo_conexao_eletrica: itemValue})}>
              <Picker.Item label="Nuevo" value="Nuevo" />
              <Picker.Item label="Antiguo" value="Antiguo" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>Cliente Dispone de Polo a Tierra?</Text>
            <Picker
              selectedValue={this.state.cliente_disponivel_polo_terra}
              prompt='Cliente Dispone de Polo a Tierra?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_disponivel_polo_terra: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>

            <TextInput value={this.state.tipo_mala_terra} placeholder="Tipo de Malla de Tierra" onChangeText={(tipo_mala_terra) => {this.setState({tipo_mala_terra: tipo_mala_terra})}} />
            <TextInput value={this.state.medida_polo_terra} placeholder="Medida del Pollo a Tierra" onChangeText={(medida_polo_terra) => {this.setState({medida_polo_terra: medida_polo_terra})}} />
            <TextInput value={this.state.potencia_saida_usar} placeholder="Potencia de Salida a Usar" onChangeText={(potencia_saida_usar) => {this.setState({potencia_saida_usar: potencia_saida_usar})}} />

            <Text style={[styles.title, styles.textTitle]}>Cliente Dispone de Espacio para Instalar La Planta GCM?</Text>
            <Picker
              selectedValue={this.state.cliente_disponivel_instalar_gcm}
              prompt='Cliente Dispone de Espacio para Instalar La Planta GCM?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_disponivel_instalar_gcm: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>

            <TextInput value={this.state.espaco_disponivel_planta_gcm} placeholder="Espacio Disponible para Instalar la Planta GCM" onChangeText={(espaco_disponivel_planta_gcm) => {this.setState({espaco_disponivel_planta_gcm: espaco_disponivel_planta_gcm})}} />

            <Text style={[styles.title, styles.textTitle]}>El Sitio del Cliente Tiene Seguridad Perimetral?</Text>
            <Picker
              selectedValue={this.state.cliente_possui_seguranca_perimetral}
              prompt='El Sitio del Cliente Tiene Seguridad Perimetral?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_possui_seguranca_perimetral: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>El Sitio de Cliente es Externo o Interno?</Text>
            <Picker
              selectedValue={this.state.cliente_externo_interno}
              prompt='El Sitio de Cliente es Externo o Interno?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_externo_interno: itemValue})}>
              <Picker.Item label="Externo" value="Externo" />
              <Picker.Item label="Interno" value="Interno" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>Podemos Instalar Placa de Concreto Para la Base?</Text>
            <Picker
              selectedValue={this.state.instalar_placa_concreto_base}
              prompt='Podemos Instalar Placa de Concreto Para la Base?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({instalar_placa_concreto_base: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>Facilidades de Acceso Al Sitio?</Text>
            <TextInput value={this.state.facilidade_acesso_carretera} placeholder="Carretera..." onChangeText={(facilidade_acesso_carretera) => {this.setState({facilidade_acesso_carretera: facilidade_acesso_carretera})}} />
            <TextInput value={this.state.facilidade_acesso_trocha} placeholder="Trocha..." onChangeText={(facilidade_acesso_trocha) => {this.setState({facilidade_acesso_trocha: facilidade_acesso_trocha})}} />
            <TextInput value={this.state.facilidade_acesso_area} placeholder="Area..." onChangeText={(facilidade_acesso_area) => {this.setState({facilidade_acesso_area: facilidade_acesso_area})}} />
            <TextInput value={this.state.facilidade_acesso_maritima} placeholder="Maritima..." onChangeText={(facilidade_acesso_maritima) => {this.setState({facilidade_acesso_maritima: facilidade_acesso_maritima})}} />


            <View style={styles.line} />
            <TextInput value={this.state.voltagem} placeholder="Voltaje Nominal..." onChangeText={(voltagem) => {this.setState({voltagem: voltagem})}} keyboardType="number-pad" />
            <TextInput value={this.state.frequencia} placeholder="Frecuencia..." onChangeText={(frequencia) => {this.setState({frequencia: frequencia})}} keyboardType="number-pad" />
            <TextInput value={this.state.numero_fases} placeholder="No. de Fases..." onChangeText={(numero_fases) => {this.setState({numero_fases: numero_fases})}} keyboardType="number-pad" />
            <TextInput value={this.state.fator_potencia} placeholder="Factor de Potencia..." onChangeText={(fator_potencia) => {this.setState({fator_potencia: fator_potencia})}} />
            <TextInput value={this.state.decibeis} placeholder="Decibeles en Sitio..." onChangeText={(decibeis) => {this.setState({decibeis: decibeis})}} keyboardType="number-pad" />
            <TextInput value={this.state.amperagem} placeholder="Amperaje en Sitio..." onChangeText={(amperagem) => {this.setState({amperagem: amperagem})}} />
            <TextInput value={this.state.medida_localizacao_fabrica_cliente} placeholder="Medida Entre la Ubicación de Nuestra Planta al Tablero Principal del Cliente..." onChangeText={(medida_localizacao_fabrica_cliente) => {this.setState({medida_localizacao_fabrica_cliente: medida_localizacao_fabrica_cliente})}} />

            <Text style={[styles.title, styles.textTitle]}>Tipo de Transmision</Text>
            <Picker
              selectedValue={this.state.tipo_transmissao}
              prompt='Tipo de Transmision' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({tipo_transmissao: itemValue})}>
              <Picker.Item label="LTS" value="LTS" />
              <Picker.Item label="LTA" value="LTA" />
              <Picker.Item label="DUCTERIA" value="DUCTERIA" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>El Cliente Tiene Tablero de Transferencia?</Text>
            <Picker
              selectedValue={this.state.cliente_possui_placa_transferencia}
              prompt='El Cliente Tiene Tablero de Transferencia?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({cliente_possui_placa_transferencia: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>

            <Text style={[styles.title, styles.textTitle]}>Estado de las Conexiones Internas?</Text>
            <Picker
              selectedValue={this.state.estado_conexoes_internas}
              prompt='Estado de las Conexiones Internas?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({estado_conexoes_internas: itemValue})}>
              <Picker.Item label="Nuevas" value="Nuevas" />
              <Picker.Item label="Antiguas" value="Antiguas" />
            </Picker>

            <TextInput value={this.state.quantidade_equipamentos_energizar} placeholder="Cantidad de Equipos a Energizar..." onChangeText={(quantidade_equipamentos_energizar) => {this.setState({quantidade_equipamentos_energizar: quantidade_equipamentos_energizar})}} keyboardType="number-pad"  />
            <TextInput value={this.state.potencia_real_energetica} placeholder="Potencia Real de Demand Energetica..." onChangeText={(potencia_real_energetica) => {this.setState({potencia_real_energetica: potencia_real_energetica})}} keyboardType="number-pad"  />
            <TextInput value={this.state.horas_pico_energetica} placeholder="Horas Pico de Demanda Energetica..." onChangeText={(horas_pico_energetica) => {this.setState({horas_pico_energetica: horas_pico_energetica})}} keyboardType="number-pad" />

            <Text style={[styles.title, styles.textTitle]}>En el Sitio Trabajan 7x24?</Text>
            <Picker
              selectedValue={this.state.trabalham_24}
              prompt='En el Sitio Trabajan 7x24?' style={{ color: colors.regular }}
              onValueChange={(itemValue, itemIndex) => this.setState({trabalham_24: itemValue})}>
              <Picker.Item label="Si" value="Si" />
              <Picker.Item label="No" value="No" />
            </Picker>
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Protocolo de servicio</Text>
						<TextInput value={this.state.protocolo} placeholder="Número de protocolo" onChangeText={(protocolo) => {this.setState({protocolo: protocolo})}} />
						<TextInput value={this.state.tecnicos_adicionais} placeholder="Técnicos adicionales" onChangeText={(tecnicos_adicionais) => {this.setState({tecnicos_adicionais: tecnicos_adicionais})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
            <Text style={[styles.blockTitle, styles.title]}>Cliente</Text>
						<TextInput value={this.state.cliente_nome} placeholder="Nombre del cliente" onChangeText={(cliente_nome) => {this.setState({cliente_nome: cliente_nome})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Correos eletrónicos que recibirán una copia de OS para la impresión y la firma. Separados por coma (,)</Text>
						<TextInput value={this.state.email_default} placeholder="Correos eletrónicos" onChangeText={(email_default) => {this.setState({email_default: email_default})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Productos utilizados. Separados por coma (,)</Text>
						<TextInput value={this.state.produtos} placeholder="Productos utilizados" onChangeText={(produtos) => {this.setState({produtos: produtos})}} />
          </View>
        </View>

        <View style={[styles.jobContainer, styles.jobContainerWithPadding]}>
          <View style={styles.row}>
						<Text style={[styles.blockTitle, styles.title]}>Para completar el proceso, las firmas son necesarias</Text>
            <View style={{ flex: 1, flexDirection: "column", alignItems: 'center' }}>
              <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._showSignatureView.bind(this)}>
                    <View style={[styles.flexCenter, {padding: 10}]}>

                      <Text style={styles.signatureText}>
                        {data ? 'Esta es su suscripción' : 'Haga clic aquí para firmar.'}
                      </Text>
                      <View style={{paddingBottom: 10}} />
                      {data &&
                        <View style={{backgroundColor: 'white', }}>
                          <Image
                            resizeMode={'contain'}
                            style={{width: 300, height: 300}}
                            source={{uri: data}}
                          />
                        </View>
                      }
                    </View>
                  </TouchableOpacity>
                  <SignatureView
                    ref={r => this._signatureView = r}
                    rotateClockwise={!!true}
                    onSave={this._onSave.bind(this)}
                  />
              </View>

              <View style={{ flex: 1, flexDirection: "row", alignItems: 'center' }}>
                  <TouchableOpacity onPress={this._showUserSignatureView.bind(this)}>
                    <View style={[styles.flexCenter, {padding: 10}]}>

                      <Text style={styles.signatureText}>
                        {userData ? 'Esta es la firma del técnico' : 'Haga clic aquí para la firma del técnico.'}
                      </Text>
                      <View style={{paddingBottom: 10}} />
                      {userData &&
                        <View style={{backgroundColor: 'white', }}>
                          <Image
                            resizeMode={'contain'}
                            style={{width: 300, height: 300}}
                            source={{uri: userData}}
                          />
                        </View>
                      }
                    </View>
                  </TouchableOpacity>
                  <SignatureView
                    ref={r => this._userSignatureView = r}
                    rotateClockwise={!!true}
                    onSave={this._onUserSave.bind(this)}
                  />
              </View>
            </View>
            <TouchableOpacity style={styles.button} onPress={() => this.saveJob()}>
              {
                this.pressed
                  ? <ActivityIndicator size="small"/>
									: <Text style={styles.buttonText}>Finalizar Atención</Text>
              }
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>);
  }
};

export default withNavigation(FinishJobEletricSiteSurveyBox);
