import axios from 'axios';
const api = axios.create({
  // baseURL: "http://192.168.0.113:8080/msa/api"
  // baseURL: "http://192.168.0.101:8080/msa/api"
  // baseURL: "http://192.168.0.103:8080/msa/api"
  // baseURL: "http://192.168.0.108:8080/msa/api"
  // baseURL: "http://192.168.129.48:8080/msa/api"
  // baseURL : "http://192.168.137.2:8080/msa/api"
  // baseURL: "http://192.168.135.242:8080/msa/api"
  // baseURL: "http://192.168.0.136:8080/msa/api"
  // baseURL: "http://192.168.0.102:8080/msa/api"
  // baseURL : "http://10.0.0.232:8080/msa/api"
  // baseURL: "http://192.168.132.128:8080/msa/api"
  baseURL: "http://app.pxos.com.br/msa/api"
  // baseURL: "http://10.0.0.104:8080/msa/api"
});

export default api;
