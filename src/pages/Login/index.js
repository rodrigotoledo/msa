/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from "react";
import { NavigationActions } from 'react-navigation';
import DeviceInfo from 'react-native-device-info';
import PropTypes from 'prop-types';
import api from 'services/api';


import { View, Text, TextInput, TouchableOpacity, AsyncStorage, StatusBar, Image, ActivityIndicator } from "react-native";

import styles from "./styles";

export default class Login extends Component {
  static navigationOptions = {
    header: null,
  };

  static propTypes = {
    navigation: PropTypes.shape({
      dispatch: PropTypes.func,
    }).isRequired,
  };

  state = {

    // email: 'admin@msa.com.br',
    // email: 'operador_msa@msa.com.br',
    password: 'asdqwe123',
    // Operador
    email: 'rodrigo.toledo@proxer.com.br',
    // password: 'asdasdasd',
    // email: '',
    // password: '',
  };

  checkUserExists = async (email, password) => {
    const user = await api.post(
      '/users/sign_in', {
        email,
        password,
        device_uuid: DeviceInfo.getUniqueID(),
        loading: false,
        errorMessage: null,
      },
      {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      },
    );
    await AsyncStorage.setItem('@ClientKey:auth_token', user.data.auth_token);
    // await AsyncStorage.setItem('@ClientKey:jobs', JSON.stringify(user.data.jobs));
    return user;
  };

  saveUser = async (email) => {
    await AsyncStorage.setItem('@ClientKey:email', email);
  };

  signIn = async () => {
    const { email, password } = this.state;

    if (email === undefined || email.length === 0 || password === undefined || password.length === 0) return;

    this.setState({ loading: true });

    try {
      await this.checkUserExists(email, password);
      await this.saveUser(email);

      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'HomeJobs' }),
        ],
      });
      this.props.navigation.dispatch(resetAction);
    } catch (err) {
      // console.tron.log(err)
      this.setState({ loading: false, errorMessage: 'Error de autenticación, compruebe los datos' });
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar barStyle="light-content" />

        <Image source={require('images/logoLogin.png')} style={styles.logo} />
        <Text style={styles.description}>Rellene los campos abajo para acceder al sistema</Text>
        { !!this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
        <View style={styles.form}>
          <TextInput autoCapitalize="none" placeholderTextColor='#6B266A' autoCorrect={false} style={styles.input} placeholder="Introduzca su correo electrónico" underlineColorAndroid="rgba(0,0,0,0)" value={this.state.email} onChangeText={email => this.setState({email})} />
          <TextInput autoCapitalize="none" placeholderTextColor='#6B266A' autoCorrect={false} style={[styles.input, styles.marginTop]} placeholder="Introduzca su contraseña" value={this.state.password} underlineColorAndroid="rgba(0,0,0,0)" secureTextEntry={true} onChangeText={password => this.setState({password})} />

          <TouchableOpacity style={styles.button} onPress={this.signIn}>
            {
              this.state.loading ? <ActivityIndicator size='small' color='#fff' /> : <Text style={styles.buttonText}>Acceder</Text>
            }
          </TouchableOpacity>
        </View>
      </View>);
  }
};
